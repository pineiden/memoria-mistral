INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django_countries',
    "apps.pagina",
    "apps.home",
    "apps.mistral_on_map",
    "apps.localizacion",
]
