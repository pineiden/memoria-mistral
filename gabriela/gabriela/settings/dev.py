from .base import *

DEBUG = True
SPATIALITE_LIBRARY_PATH = 'mod_spatialite'

engine = 'django.contrib.gis.db.backends.spatialite'
DATABASES = {
    'default': {
        'ENGINE': engine,
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

SITE_URL = "www.memoriamistral.cl"
SITE_NAME = "Arte Gabriel Mistral"
GOOGLE_API_KEY = ''
SITE_ID = 1
ALLOWED_HOSTS = ["*"]
