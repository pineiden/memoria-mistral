from .base import *

DEBUG = False
# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DBNAME = get_env_variable('DBNAME')
DBUSER = get_env_variable('DBUSER')
DBPASS = get_env_variable('DBPASS')
engine = 'django.contrib.gis.db.backends.postgis'
DATABASES = {
    'default': {
        'ENGINE': engine,
        'NAME': DBNAME,
        'USER': DBUSER,
        'PASSWORD': DBPASS,
        'HOST': '',
        'PORT': '',
    },
}

SITE_URL = "www.memoriamistral.cl"
SITE_NAME = "Arte Gabriel Mistral"
GOOGLE_API_KEY = ''

ALLOWED_HOSTS = ['localhost',
                 'memoriamistral.cl', '*']
SITE_ID = 1
