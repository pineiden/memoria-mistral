var map = L.map('mapid').setView([-33.447487, -70.673676], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// Lista de objetos: lista_artes

lista_artes.forEach(function(element){
    console.log("Recorriendo lista de artes");
    console.log(element);
    L.marker(element.posicion).addTo(map)
        .bindPopup(element.nombre)
        .openPopup();

})
