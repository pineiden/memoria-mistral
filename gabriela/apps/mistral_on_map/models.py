from apps.localizacion.models import Localizacion
from django.contrib.gis.db import models
from django.utils.text import slugify
import json


def get_upload_pagina_file_name(instance, filename):
    return "paginas/%s/%s" % (instance.slug_titulo, filename)

# Create your models here.


"""
Es una aplicacion que permite registrar imágenes
posicionadas geograficamente
"""


class TipoArte(models.Model):
    tipo = models.CharField(
        max_length=20,
        unique=True)
    slug_tipo = models.SlugField(unique=True, blank=True)
    descripcion = models.TextField(blank=True)

    def get_absolute_url(self):
        return reverse('mistral_on_map:clasificacion',
                       kwargs={'slug_tipo': self.slug_tipo})

    def save(self, *args, **kwargs):
        self.slug_tipo = slugify(self.tipo)
        # TODO: check if exists before save
        super().save(*args, **kwargs)

    @property
    def Clasificacion(self):
        return self.tipo

    class Meta:
        app_label = "mistral_on_map"
        verbose_name = "Tipo de Arte"
        verbose_name_plural = "Tipos de Arte"
        ordering = ("tipo",)

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.tipo


class ArteOnMap(models.Model):
    titulo = models.CharField(max_length=100)
    slug_titulo = models.SlugField(blank=True)
    imagen = models.ImageField(
        upload_to=get_upload_pagina_file_name,
        blank=True)
    fecha_publicacion = models.DateTimeField(auto_now=True)
    cuerpo = models.TextField(help_text="Escribe todo el contenido")
    posicion = models.ForeignKey(
        Localizacion,
        on_delete=models.CASCADE,
        blank=True,
        null=True)
    clasificacion = models.ForeignKey(
        TipoArte,
        on_delete=models.CASCADE,
        blank=True,
        null=True)

    def get_absolute_url(self):
        return reverse('mistral_on_map:detalle_pagina',
                       kwargs={'slug': self.slug_titulo})

    def save(self, *args, **kwargs):
        self.slug_titulo = slugify(self.titulo)
        # TODO: check if exists before save
        super().save(*args, **kwargs)

    @property
    def Pagina(self):
        return self.titulo

    class Meta:
        app_label = "mistral_on_map"
        verbose_name = "Arte en el Mapa"
        verbose_name_plural = "Arte en el Mapa"
        ordering = ("clasificacion", "fecha_publicacion")

    def __str__(self):
        return self.titulo

    def __repr__(self):
        return json.dumps(self.__dict__)
