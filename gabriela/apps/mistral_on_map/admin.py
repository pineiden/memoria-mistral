from django.contrib import admin
from apps.mistral_on_map.models import TipoArte, ArteOnMap
# Register your models here.


class AdminTipoArte(admin.ModelAdmin):
    prepopulated_fields = {"slug_tipo": ("tipo",)}
    model = TipoArte


class AdminArteOnMap(admin.ModelAdmin):
    prepopulated_fields = {"slug_titulo": ("titulo",)}
    model = ArteOnMap


admin.site.register(TipoArte, AdminTipoArte)
admin.site.register(ArteOnMap, AdminArteOnMap)
