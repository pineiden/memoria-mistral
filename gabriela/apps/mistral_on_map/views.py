from django.shortcuts import render
from django.views.generic import ListView
from apps.mistral_on_map.models import ArteOnMap
# Create your views here.

"""
The map with the images in short are a list of element in a mapa
"""


class VistaMapArteMistral(ListView):
    model = ArteOnMap
    template_name = 'mistral_on_map/mapa.dj.html'
