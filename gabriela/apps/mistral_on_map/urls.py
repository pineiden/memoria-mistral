from django.conf.urls import url
from django.urls import path

from apps.mistral_on_map.views import VistaMapArteMistral

app_name = 'mistral_on_map'


urlpatterns = [
    path('mapa/',
         VistaMapArteMistral.as_view(),
         name='mapa_arte_mistral'),
]
