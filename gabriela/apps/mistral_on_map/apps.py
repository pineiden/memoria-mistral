from django.apps import AppConfig


class MistralOnMapConfig(AppConfig):
    name = 'mistral_on_map'
