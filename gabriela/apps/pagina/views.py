from django.shortcuts import render
from django.views.generic import (DetailView,
                                  ListView,
                                  CreateView,
                                  UpdateView,
                                  DeleteView,
                                  TemplateView)
from .models import (Clasificacion, Pagina)
from django.urls import reverse, reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin

# Create your views here.


class VistaPagina(DetailView):
    model = Pagina
    slug_field = 'slug_titulo'
    template_name = 'pagina/pagina.dj.html'


class ListarPaginas(ListView):
    model = Pagina
    template_name = 'pagina/lista_paginas.dj.html'
    allow_empty = True
    paginate_by = 20


class CrearPagina(CreateView):
    model = Pagina
    template_name = 'pagina/edita_pagina.dj.html'
    slug_field = 'slug_titulo'

    def get_success_url(self):
        return reverse_lazy(
            'edicion_exitosa/',
            args=(self.object.id))


class EditaPagina(UpdateView):
    model = Pagina
    template_name = 'pagina/edita_pagina.dj.html'
    slug_field = 'slug_titulo'

    def get_success_url(self):
        return reverse_lazy(
            'edicion_exitosa/',
            args=(self.object.id))


class EliminaPagina(DeleteView):
    model = Pagina
    template_name = 'pagina/elimina_pagina.dj.html'
    slug_field = 'slug_titulo'

    def get_success_url(self):
        return reverse_lazy(
            'eliminacion_exitosa/',
            args=(self.object.id))


class EdicionPaginaExitosa(VistaPagina):
    template_name = 'pagina/edicion_pagina_exitosa.dj.html'


class EliminacionPaginaExitosa(TemplateView):
    template_name = 'pagina/eliminacion_pagina_exitosa.dj.html'
